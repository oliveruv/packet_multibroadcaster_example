
//----------------------------------------------------------------------------------------------
/*  Purpose of File:


    Implementation:


    API:
        INIT:

        UI:

*/
//----------------------------------------------------------------------------------------------
package audio;

//  Native Java Classes
import javax.sound.sampled.TargetDataLine;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;

//  Local Java Classes
import constants.AudioConstants;

//  Library Java Classes
import com.cattura.packet_multibroadcaster.implementations.Source;
import com.cattura.packet_multibroadcaster.constants.AudioVideoTypes;
import com.cattura.packet_multibroadcaster.value_objects.AudioPacket;

public class MicrophoneAudioSource extends Source
{
    //  Constants
    private final byte[] TEMP_BUFFER;

    //  Static variables
    //  Instance variables
    private TargetDataLine _targetDataLine;

    //----------------------------------------------------------------------------------------------
    //  Initialize
    //----------------------------------------------------------------------------------------------
    public MicrophoneAudioSource (String $id)
    {
        super($id, AudioVideoTypes.AUDIO);

        TEMP_BUFFER = new byte[AudioConstants.TARGET_DATA_LINE_BYTE_SIZE];

		try
		{
			_targetDataLine = (TargetDataLine)AudioSystem.getLine(new DataLine.Info(TargetDataLine.class, AudioConstants.CATTURA_CONFIGURED_AUDIO_FORMAT));
            _targetDataLine.open(AudioConstants.CATTURA_CONFIGURED_AUDIO_FORMAT);
            _targetDataLine.start();
		}
		catch (LineUnavailableException $ignore)
		{
		}
    }

    //----------------------------------------------------------------------------------------------
    //  Functions
    //----------------------------------------------------------------------------------------------
    protected void packAudioPacket (AudioPacket $audioPacket)
    {
        if (_targetDataLine.read(TEMP_BUFFER, 0, TEMP_BUFFER.length) > 0)
        {
            $audioPacket.pack(System.nanoTime(), TEMP_BUFFER);
        }
    }

    //----------------------------------------------------------------------------------------------
    //  Setter
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    //  Getter
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    //  Event Handlers
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    //  Helper Functions
    //----------------------------------------------------------------------------------------------
    
    //----------------------------------------------------------------------------------------------
    //  Return Functions
    //----------------------------------------------------------------------------------------------
}