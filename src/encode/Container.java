
//----------------------------------------------------------------------------------------------
/*  Purpose of File:


    Implementation:


    API:
        INIT:

        UI:

*/
//----------------------------------------------------------------------------------------------
package encode;

//  Native Java Classes
import java.io.File;
import java.util.concurrent.TimeUnit;
import java.awt.image.BufferedImage;

//  Local Java Classes
import constants.VideoConstants;
import constants.EncodingConstants;
import constants.AudioConstants;

//  Library Java Classes
import com.cattura.packet_multibroadcaster.implementations.Writer;
import com.cattura.packet_multibroadcaster.value_objects.AudioPacket;
import com.cattura.packet_multibroadcaster.value_objects.VideoPacket;
import com.xuggle.mediatool.IMediaWriter;
import com.xuggle.mediatool.ToolFactory;
import com.xuggle.xuggler.IAudioSamples;
import com.xuggle.xuggler.ICodec;
import com.xuggle.ferry.IBuffer;
import com.xuggle.xuggler.Global;
import com.xuggle.xuggler.video.ConverterFactory;

public class Container extends Writer
{
    //  Constants
    private static final TimeUnit NANOSECONDS_TIME_UNIT = TimeUnit.NANOSECONDS;
    
    //  Static variables
    //  Instance variables
    private IMediaWriter _mediaWriter;

    //----------------------------------------------------------------------------------------------
    //  Initialize
    //----------------------------------------------------------------------------------------------
    public Container (String $ouputDirectory, String $filename, String $extension, String $type)
    {
        super($ouputDirectory, $filename, $extension, $type);
    }

    //----------------------------------------------------------------------------------------------
    //  Functions
    //----------------------------------------------------------------------------------------------
    public void setupProcessing ()
    {
        super.setupProcessing();

        _mediaWriter = ToolFactory.makeWriter(getOutputDirectory() + File.separator + getFilename() + "." + getExtension());
        _mediaWriter.open();
        _mediaWriter.setForceInterleave(true);

        if (getSupportsVideo())
        {
            _mediaWriter.addVideoStream(EncodingConstants.VIDEO_STREAM_ID, 0, ICodec.guessEncodingCodec(null, null, getOutputDirectory() + File.separator + getFilename() + "." + getExtension(), null, ICodec.Type.CODEC_TYPE_VIDEO), VideoConstants.SCREEN_WIDTH, VideoConstants.SCREEN_HEIGHT);
        }

        if (getSupportsAudio())
        {
            _mediaWriter.addAudioStream(EncodingConstants.AUDIO_STREAM_ID, 0, ICodec.guessEncodingCodec(null, null, getOutputDirectory() + File.separator + getFilename() + "." + getExtension(), null, ICodec.Type.CODEC_TYPE_AUDIO), AudioConstants.NUMBER_OF_CHANNELS, AudioConstants.SAMPLE_RATE_AS_INT);
        }
    }

    public void finalizeProcessing ()
    {
        super.finalizeProcessing();

        _mediaWriter.flush();
        _mediaWriter.close();
    }

    public void encodeVideo (VideoPacket $packet)
    {
        _mediaWriter.encodeVideo(EncodingConstants.VIDEO_STREAM_ID, ConverterFactory.convertToType($packet.getBufferedImage(), BufferedImage.TYPE_3BYTE_BGR), $packet.getTimestamp(), NANOSECONDS_TIME_UNIT);
    }

    public void encodeAudio (AudioPacket $packet)
    {
        byte[] audioSamplesByteArray = $packet.getAudioSamplesAsByteArray();
        int audioSamplesByteArrayLength = audioSamplesByteArray.length;

        if (audioSamplesByteArrayLength > 0)
        {
            IBuffer audioBuffer = IBuffer.make(null, audioSamplesByteArray, 0, audioSamplesByteArrayLength);
            audioBuffer.setType(IBuffer.Type.IBUFFER_SINT16);

            IAudioSamples audioSamples = IAudioSamples.make(audioBuffer, 1, IAudioSamples.Format.FMT_S16);
            audioSamples.setComplete(true, audioBuffer.getSize(), AudioConstants.SAMPLE_RATE_AS_INT, 1, IAudioSamples.Format.FMT_S16, Global.NO_PTS);

            _mediaWriter.encodeAudio(EncodingConstants.AUDIO_STREAM_ID, audioSamples);
        }
    }

    //----------------------------------------------------------------------------------------------
    //  Setter
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    //  Getter
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    //  Event Handlers
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    //  Helper Functions
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    //  Return Functions
    //----------------------------------------------------------------------------------------------
}