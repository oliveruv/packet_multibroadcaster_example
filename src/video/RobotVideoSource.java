
//----------------------------------------------------------------------------------------------
/*  Purpose of File:


    Implementation:


    API:
        INIT:

        UI:

*/
//----------------------------------------------------------------------------------------------
package video;

//  Native Java Classes
import java.awt.Robot;
import java.awt.Rectangle;

//  Local Java Classes
//  Library Java Classes
import com.cattura.packet_multibroadcaster.implementations.Source;
import com.cattura.packet_multibroadcaster.constants.AudioVideoTypes;
import com.cattura.packet_multibroadcaster.value_objects.VideoPacket;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

public class RobotVideoSource extends Source
{
    //  Constants
    private final Rectangle SCREEN_RECTANGLE;
    private final int SCREEN_WIDTH;
    private final int SCREEN_HEIGHT;
    private int iter = 0;
    private final String OUTPUT_DIRECTORY_PATH = System.getProperty("user.home") + File.separator + "Desktop" + File.separator;
    
    //  Static variables
    //  Instance variables
    private Robot _robot;

    //----------------------------------------------------------------------------------------------
    //  Initialize
    //----------------------------------------------------------------------------------------------
    public RobotVideoSource (String $id, int $screenWidth, int $screenHeight) throws Exception
    {
        super($id, AudioVideoTypes.VIDEO);

        SCREEN_WIDTH = $screenWidth;
        SCREEN_HEIGHT = $screenHeight;
        SCREEN_RECTANGLE = new Rectangle(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

        _robot = new Robot();
    }

    //----------------------------------------------------------------------------------------------
    //  Functions
    //----------------------------------------------------------------------------------------------
    protected void packVideoPacket (VideoPacket $videoPacket)
    {
        BufferedImage ss = _robot.createScreenCapture(SCREEN_RECTANGLE);
        File outputfile = new File(OUTPUT_DIRECTORY_PATH + "image_" + Integer.toString(iter) + ".jpg");
        iter++;
        try {
            ImageIO.write(ss, "jpg", outputfile);
        } catch (IOException ex) {
            Logger.getLogger(RobotVideoSource.class.getName()).log(Level.SEVERE, null, ex);
        }
        $videoPacket.pack(System.nanoTime(), ss);
    }

    //----------------------------------------------------------------------------------------------
    //  Setter
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    //  Getter
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    //  Event Handlers
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    //  Helper Functions
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    //  Return Functions
    //----------------------------------------------------------------------------------------------
}