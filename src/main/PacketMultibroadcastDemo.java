
//----------------------------------------------------------------------------------------------
/*  Purpose of File:


    Implementation:


    API:
        INIT:

        UI:

*/
//----------------------------------------------------------------------------------------------
package main;

//  Native Java Classes
import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

//  Local Java Classes
import audio.MicrophoneAudioSource;
import video.RobotVideoSource;
import constants.VideoConstants;
import encode.Container;

//  Library Java Classes
import com.cattura.packet_multibroadcaster.implementations.PacketMultibroadcaster;
import com.cattura.packet_multibroadcaster.implementations.SourceGroup;
import com.cattura.packet_multibroadcaster.constants.AudioVideoTypes;

public class PacketMultibroadcastDemo
{
    //  Constants
    private final String AUDIO_TYPE = AudioVideoTypes.AUDIO;
    private final String VIDEO_TYPE = AudioVideoTypes.VIDEO;
    private final String AUDIO_AND_VIDEO_TYPE = AudioVideoTypes.AUDIO_AND_VIDEO;
    private final String OUTPUT_DIRECTORY_PATH = System.getProperty("user.home") + File.separator + "Desktop" + File.separator;
    private final int CAPTURE_DURATION = 10000;

    //  Static variables
    //  Instance variables

    //----------------------------------------------------------------------------------------------
    //  Initialize
    //----------------------------------------------------------------------------------------------
    public static void main (String[] args)
    {
        new PacketMultibroadcastDemo();
    }

    public PacketMultibroadcastDemo ()
    {
        RobotVideoSource robotVideoSource = null;
        MicrophoneAudioSource microphoneAudioSource = null;

        try
        {
            robotVideoSource = new RobotVideoSource("RobotVideoSource", VideoConstants.SCREEN_WIDTH, VideoConstants.SCREEN_HEIGHT);
            microphoneAudioSource = new MicrophoneAudioSource("MicrophoneAudioSource");
        }
        catch (Exception $ignore)
        {
        }
        final SourceGroup sourceGroup = PacketMultibroadcaster.makeSourceGroup(robotVideoSource, microphoneAudioSource);

        PacketMultibroadcaster.registerWriterToSourceGroup(sourceGroup, new Container(OUTPUT_DIRECTORY_PATH, "video_file", "mp4", VIDEO_TYPE));
        PacketMultibroadcaster.registerWriterToSourceGroup(sourceGroup, new Container(OUTPUT_DIRECTORY_PATH, "audio_file", "mp4", AUDIO_TYPE));
        PacketMultibroadcaster.registerWriterToSourceGroup(sourceGroup, new Container(OUTPUT_DIRECTORY_PATH, "audio_video_file", "mp4", AUDIO_AND_VIDEO_TYPE));
//        PacketMultibroadcaster.registerWriterToSourceGroup(sourceGroup, new Container(OUTPUT_DIRECTORY_PATH, "audio_file_1", "mp3", AUDIO_TYPE));
//        PacketMultibroadcaster.registerWriterToSourceGroup(sourceGroup, new Container(OUTPUT_DIRECTORY_PATH, "audio_file_2", "mp3", AUDIO_TYPE));
//        PacketMultibroadcaster.registerWriterToSourceGroup(sourceGroup, new Container(OUTPUT_DIRECTORY_PATH, "audio_file_3", "mp3", AUDIO_TYPE));
//        PacketMultibroadcaster.registerWriterToSourceGroup(sourceGroup, new Container(OUTPUT_DIRECTORY_PATH, "video_file_1", "mov", VIDEO_TYPE));
//        PacketMultibroadcaster.registerWriterToSourceGroup(sourceGroup, new Container(OUTPUT_DIRECTORY_PATH, "video_file_2", "mov", VIDEO_TYPE));
//        PacketMultibroadcaster.registerWriterToSourceGroup(sourceGroup, new Container(OUTPUT_DIRECTORY_PATH, "video_file_3", "mov", VIDEO_TYPE));

        SourceGroup.setupProcessingOnAllSources();
        SourceGroup.beginProcessingOnAllSources();

        (new Timer()).schedule(new TimerTask()
        {
            public void run ()
            {
                SourceGroup.stopProcessingOnAllSources();
            }
        }, CAPTURE_DURATION);
    }

    //----------------------------------------------------------------------------------------------
    //  Functions
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    //  Setter
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    //  Getter
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    //  Event Handlers
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    //  Helper Functions
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    //  Return Functions
    //----------------------------------------------------------------------------------------------
}